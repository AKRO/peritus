package akropol.peritus.controller;

import java.io.File;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import akropol.peritus.forms.AdminForm;
import akropol.peritus.forms.LoginForm;
import akropol.peritus.hibernate.DbOperations;
import akropol.peritus.hibernate.NativeHibernateViaXML;
import akropol.peritus.utils.Converter;
import akropol.peritus.utils.Dictionary;

@Controller
public class ApplicationController {

	NativeHibernateViaXML nativeHibernateViaXML = new NativeHibernateViaXML();
	DbOperations dbOperations = new DbOperations();

	// @RequestMapping(value="/loginPage", method=RequestMethod.GET)
	@RequestMapping(method = RequestMethod.GET)
	public String getLoginForm() throws Exception {
		nativeHibernateViaXML.setUp();
		dbOperations.setSessionFactory(nativeHibernateViaXML.getSessionFactory());

		return Dictionary.LOGIN_PAGE;
	}

	@RequestMapping(value = Dictionary.LOGIN_PAGE_URL, method = RequestMethod.POST)
	public String login(@ModelAttribute(name = Dictionary.LOGIN_FORM) LoginForm loginFrom, Model model)
			throws SQLException {
		String username = loginFrom.getUsername();
		String password = loginFrom.getPassword();

		if (username.equals(Dictionary.ADMIN) && password.equals(Dictionary.ADMIN)) {
			return Dictionary.ADMIN_PAGE;
		} else if (username.equals(Dictionary.ADMIN) && !password.equals(Dictionary.ADMIN)) {
			model.addAttribute(Dictionary.INVALID_CREDENTIALS, true);

			return Dictionary.LOGIN_PAGE;
		} else {
			List<Blob> blobList = fillUserGallery(username);
			Converter converter = new Converter();
			model.addAttribute("photos", converter.blobToBase64(blobList));

			return Dictionary.USER_PAGE;
		}
	}

	@RequestMapping(value = Dictionary.ADMIN_PAGE, method = RequestMethod.POST, params = Dictionary.BUTTON_ADD_USER)
	public String addUser(@ModelAttribute(name = Dictionary.ADMIN_FORM) AdminForm adminForm, Model model) {
		if (adminForm.getNewUser() != null && adminForm.getNewUser() != Dictionary.EMPTY
				&& adminForm.getUserPassword() != null && adminForm.getUserPassword() != Dictionary.EMPTY) {
			dbOperations.addUser(adminForm.getNewUser(), adminForm.getUserPassword(), adminForm.getMakeAdmin());
			model.addAttribute(Dictionary.USER_ADD_OK, true);
		} else {
			model.addAttribute(Dictionary.USER_ADD_ERR, true);
		}

		return Dictionary.ADMIN_PAGE;
	}

	@RequestMapping(value = Dictionary.ADMIN_PAGE, method = RequestMethod.POST, params = Dictionary.BUTTON_ADD_PHOTO)
	public String addPhoto(@ModelAttribute(name = Dictionary.ADMIN_FORM) AdminForm adminForm,
			@RequestParam("imageFile") File file, Model model) {
		if (adminForm.getOwner() != null && adminForm.getOwner() != Dictionary.EMPTY) {
			dbOperations.addPhoto(adminForm.getOwner(), file);
			model.addAttribute(Dictionary.PHOTO_ADD_OK, true);
		} else {
			model.addAttribute(Dictionary.PHOTO_ADD_ERR, true);
		}
		return Dictionary.ADMIN_PAGE;
	}

	@RequestMapping(value = Dictionary.ADMIN_PAGE, method = RequestMethod.POST, params = Dictionary.BUTTON_ADD_GALLERY)
	public String addGallery(@ModelAttribute(name = Dictionary.ADMIN_FORM) AdminForm adminForm, Model model) {
		if (adminForm.getNewGallery() != null && adminForm.getNewGallery() != Dictionary.EMPTY
				&& adminForm.getGalleryOwner() != null && adminForm.getGalleryOwner() != Dictionary.EMPTY) {
			dbOperations.addGallery(adminForm.getGalleryOwner(), adminForm.getNewGallery());
			model.addAttribute(Dictionary.GALLERY_ADD_OK, true);
		} else {
			model.addAttribute(Dictionary.GALLERY_ADD_ERR, true);
		}
		return Dictionary.ADMIN_PAGE;
	}

	private List<Blob> fillUserGallery(String owner) {
		return dbOperations.retrieveUserGallery(owner);
	}

}
