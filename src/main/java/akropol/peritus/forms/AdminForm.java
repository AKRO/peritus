package akropol.peritus.forms;

public class AdminForm {
	private String newUser;
	private String userPassword;
	private String owner;
	private String pathToPhoto;
	private Boolean makeAdmin;
	private String newGallery;
	private String galleryOwner;

	public AdminForm() {
		super();
	}

	public String getNewUser() {
		return newUser;
	}

	public void setNewUser(String newUser) {
		this.newUser = newUser;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPathToPhoto() {
		return pathToPhoto;
	}

	public void setPathToPhoto(String pathToPhoto) {
		this.pathToPhoto = pathToPhoto;
	}

	public Boolean getMakeAdmin() {
		return makeAdmin;
	}

	public void setMakeAdmin(Boolean makeAdmin) {
		this.makeAdmin = makeAdmin;
	}

	public String getNewGallery() {
		return newGallery;
	}

	public void setNewGallery(String newGallery) {
		this.newGallery = newGallery;
	}

	public String getGalleryOwner() {
		return galleryOwner;
	}

	public void setGalleryOwner(String galleryOwner) {
		this.galleryOwner = galleryOwner;
	}

}
