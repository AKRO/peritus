package akropol.peritus.utils;

public class Dictionary {
	public static final String LOGIN_PAGE = "loginPage";
	public static final String LOGIN_PAGE_URL = "/loginPage";
	public static final String LOGIN_FORM = "loginForm";

	public static final String ADMIN = "admin";

	public static final String ADMIN_PAGE = "adminPage";
	public static final String ADMIN_PAGE_URL = "/adminPage";
	public static final String ADMIN_FORM = "adminForm";

	public static final String USER_PAGE = "userPage";

	public static final String PHOTO_ADD_OK = "photoAdditionSuccess";
	public static final String PHOTO_ADD_ERR = "photoAdditionError";
	public static final String USER_ADD_OK = "userAdditionSuccess";
	public static final String USER_ADD_ERR = "userAdditionError";
	public static final String GALLERY_ADD_OK = "galleryAdditionSuccess";
	public static final String GALLERY_ADD_ERR = "galleryAdditionError";

	public static final String EMPTY = "";
	public static final String BUTTON_ADD_USER = "buttonAddUser";
	public static final String BUTTON_ADD_PHOTO = "buttonAddPhoto";
	public static final String BUTTON_ADD_GALLERY = "buttonAddGallery";
	public static final String INVALID_CREDENTIALS = "invalidCredentials";
	public static final String IMG_LCL_PATH = "D:\\Peritus\\";
	public static final String JPG = "jpg";
}
