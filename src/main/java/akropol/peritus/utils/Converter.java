package akropol.peritus.utils;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Converter {
	public List<byte[]> blobToBase64(List<Blob> blobList) throws SQLException {
		List<byte[]> byteList = new ArrayList<byte[]>();

		for (Blob element : blobList) {
			int blobLength = (int) element.length();
			byte[] blobAsBytes = element.getBytes(1, blobLength);
			byteList.add(blobAsBytes);
		}

		return byteList;
	}

}
