package akropol.peritus.model;

import javax.persistence.Entity;

@Entity
public class UserTable {
	private int id;
	private String username;
	private String password;
	private boolean admin;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public UserTable() {
		// this form used by Hibernate
	}

	public UserTable(String username, String password, boolean admin) {
		this.username = username;
		this.password = password;
		this.admin = admin;
	}
}
