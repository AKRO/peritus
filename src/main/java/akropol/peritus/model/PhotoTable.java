package akropol.peritus.model;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class PhotoTable {
	private int id;
	private String photoName;
	private String gallery;
	@Column(name = "photo", unique = false, nullable = false, length = 1000000)
	private Blob photo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getGallery() {
		return gallery;
	}

	public void setGallery(String gallery) {
		this.gallery = gallery;
	}

	public Blob getPhoto() {
		return photo;
	}

	public void setPhoto(Blob photo) {
		this.photo = photo;
	}

	public PhotoTable() {
		// this form used by Hibernate
	}

	public PhotoTable(String photoName, String gallery) {
		this.photoName = photoName;
		this.gallery = gallery;
	}

	public PhotoTable(String gallery, Blob photo) {
		this.gallery = gallery;
		this.photo = photo;
	}

}
