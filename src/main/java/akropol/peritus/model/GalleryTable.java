package akropol.peritus.model;

import javax.persistence.Entity;

@Entity
public class GalleryTable {
	private int id;
	private String owner;
	private String galleryName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getGalleryName() {
		return galleryName;
	}

	public void setGalleryName(String galleryName) {
		this.galleryName = galleryName;
	}

	public GalleryTable() {
		// this form used by Hibernate
	}

	public GalleryTable(String owner, String galleryName) {
		this.owner = owner;
		this.galleryName = galleryName;
	}

}
