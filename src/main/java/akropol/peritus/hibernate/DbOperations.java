package akropol.peritus.hibernate;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.util.List;

import javax.imageio.ImageIO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.jdbc.BlobProxy;

import akropol.peritus.model.GalleryTable;
import akropol.peritus.model.PhotoTable;
import akropol.peritus.model.UserTable;
import akropol.peritus.utils.Dictionary;

public class DbOperations {
	SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	NativeHibernateViaXML nativeHibernateViaXML = new NativeHibernateViaXML();

	public void addUser(String user, String password, Boolean admin) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		// TODO: add proper creation of user with checking for admin property
		if (admin == null) {
			admin = false;
		}
		session.save(new UserTable(user, password, admin));
		session.getTransaction().commit();
		session.close();
	}

	public void addPhoto(String gallery, File photo) {
		String pathToPhoto = Dictionary.IMG_LCL_PATH + photo.getName();
		File file = new File(pathToPhoto);
		byte[] barr = null;
		if (file.exists()) {
			try {
				BufferedImage bufferedImage = ImageIO.read(file);
				ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
				ImageIO.write(bufferedImage, Dictionary.JPG, byteOutStream);
				barr = byteOutStream.toByteArray();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(new PhotoTable(gallery, BlobProxy.generateProxy(barr)));
		session.getTransaction().commit();
		session.close();
	}

	public void addPhoto(String photoName, String galleryName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(new PhotoTable(photoName, galleryName));
		session.getTransaction().commit();
		session.close();
	}

	public void addGallery(String owner, String galleryName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(new GalleryTable(owner, galleryName));
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Blob> retrieveUserGallery(String owner) {
		String hql;
		if (owner.equals(Dictionary.ADMIN)) {
			hql = "SELECT photo FROM GalleryTable";
		} else {
			hql = "SELECT photo FROM GalleryTable WHERE owner='" + owner + "'";
		}

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Blob> results = session.createQuery(hql).list();
		session.getTransaction().commit();
		session.close();
		return results;
	}

}
